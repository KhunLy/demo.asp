﻿using Demo.DAL.Entities;
using Demo.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDALTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DishService service = new DishService();
            Dish d = new Dish
            {
                Name = "Sushi au Saumon",
                Price = 3.3M,
                CategoryId = 1
            };
            service.Insert(d);
            var res = service.GetAll();
        }
    }
}
