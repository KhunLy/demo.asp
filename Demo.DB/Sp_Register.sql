﻿CREATE PROCEDURE [dbo].[Sp_Register]
	@email NVARCHAR(255),
	@password NVARCHAR(255),
	@lastname NVARCHAR(100),
	@firstname NVARCHAR(100),
	@birthdate Datetime2,
	@street NVARCHAR(255),
	@number NVARCHAR(10),
	@zipcode INT,
	@city NVARCHAR(100)
AS
	INSERT INTO [User]
	(Email, [Password], LastName, FirstName, BirthDate, Street, Number, ZipCode, City)
	VALUES(
		@email,
		dbo.Udf_Hash_Password(@password, @email),
		@lastname,
		@firstname,
		@birthdate,
		@street,
		@number,
		@zipcode,
		@city
	)
RETURN 0
