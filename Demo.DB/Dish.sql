﻿CREATE TABLE [dbo].[Dish]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(100) NOT NULL,
	[CategoryId] INT REFERENCES Category NOT NULL,
	[Price] DECIMAL(6,2) NOT NULL,
	[Discount] INT NULL,
	[Picture] NVARCHAR(MAX) NULL,
	[Active] BIT NOT NULL DEFAULT 1,
	[Deleted] BIT NOT NULL DEFAULT 0
)
