﻿CREATE TRIGGER [TR_Category_Delete]
ON [dbo].[Category]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON
	UPDATE Category SET Deleted = 1
	WHERE Id in (SELECT Id FROM deleted);
	UPDATE Dish SET Deleted = 1
	WHERE CategoryId IN (SELECT Id FROM deleted);
END
