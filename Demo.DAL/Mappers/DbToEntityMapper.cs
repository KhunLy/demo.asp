﻿using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Mappers
{
    class DbToEntityMapper
    {
        public static Dish ToDish(IDataReader reader)
        {
            return new Dish
            {
                Id = (int)reader["Id"],
                Name = reader["Name"].ToString(),
                Price = (decimal)reader["Price"],
                CategoryId = (int)reader["CategoryId"],
                Active = (bool)reader["Active"],
                Discount = reader["Discount"] as int?,
                Picture = reader["Picture"] as string
            };
        }

        public static User ToUser(IDataReader reader)
        {
            return new User
            {
                Id = (int)reader["Id"],
                Email = (string)reader["Email"],
                Street = (string)reader["Street"],
                ZipCode = (int)reader["ZipCode"],
                City = (string)reader["City"],
                BirthDate = reader["BirthDate"] as DateTime?,
                Role = (string)reader["Role"],
                Number = reader["Number"] as string,
                LastName = (string)reader["LastName"],
                FirstName = (string)reader["FirstName"],
            };
        }

        public static Category ToCategory(IDataReader reader)
        {
            Category c = new Category();
            c.Id = (int)reader["Id"];
            c.Name = (string)reader["Name"];
            return c;
            //return new Category
            //{
            //    Id = (int)reader["Id"],
            //    Name = (string)reader["Name"]
            //};
        }
    }
}
