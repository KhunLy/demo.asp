﻿using DB;
using Demo.DAL.Entities;
using Demo.DAL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Services
{
    public class DishService: BaseService<Dish>
    {
        public override Dish Get(int Id)
        {
            string query = "SELECT * FROM Dish WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", Id);
            return _Connection.ExecuteReader(cmd, DbToEntityMapper.ToDish).FirstOrDefault();
        }

        public int GetCountByCategory(int id)
        {
            string query = "SELECT COUNT(*) FROM Dish WHERE CategoryId = @id";
            Command cmd = new Command(query);
            cmd.AddParameter("@id", id);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override IEnumerable<Dish> GetAll()
        {
            string query = "SELECT * FROM Dish WHERE Deleted = 0";
            Command cmd = new Command(query);
            return _Connection.ExecuteReader(cmd, DbToEntityMapper.ToDish);
        }

        public override int Insert(Dish dish)
        {
            string query = "INSERT INTO Dish(Name, Price, Discount, Picture, CategoryId) OUTPUT inserted.Id" +
                " VALUES (@name, @price, @discount, @picture, @categoryId)";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@name", dish.Name);
            cmd.Parameters.Add("@price", dish.Price);
            cmd.Parameters.Add("@discount", (object)dish.Discount ?? DBNull.Value);
            cmd.Parameters.Add("@picture", (object)dish.Picture ?? DBNull.Value);
            cmd.Parameters.Add("@categoryId", dish.CategoryId);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Dish dish)
        {
            string query = "UPDATE Dish Set Name = @name, Price= @price, Discount = @discount," +
                "Picture = @picture, CategoryId = @categoryId WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@name", dish.Name);
            cmd.Parameters.Add("@price", dish.Price);
            cmd.Parameters.Add("@discount", (object)dish.Discount ?? DBNull.Value);
            cmd.Parameters.Add("@picture", (object)dish.Picture ?? DBNull.Value);
            cmd.Parameters.Add("@categoryId", dish.CategoryId);
            cmd.Parameters.Add("@id", dish.Id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override bool Delete(int id)
        {
            string query = "DELETE FROM Dish WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.AddParameter("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }
    }
}
