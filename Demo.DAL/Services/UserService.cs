﻿using DB;
using Demo.DAL.Entities;
using Demo.DAL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Services
{
    public class UserService : BaseService<User>
    {
        public override bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override User Get(int id)
        {
            Command cmd = new Command("SELECT * FROM [User] WHERE id = @id");
            cmd.AddParameter("@id", id);
            return _Connection
                .ExecuteReader(cmd, DbToEntityMapper.ToUser)
                .FirstOrDefault();
        }

        public override IEnumerable<User> GetAll()
        {
            Command cmd = new Command("SELECT * FROM [User]");
            return _Connection.ExecuteReader(cmd, DbToEntityMapper.ToUser);
        }

        public override int Insert(User item)
        {
            Command cmd = new Command("Sp_Register", true);
            cmd.AddParameter("@email", item.Email);
            cmd.AddParameter("@password", item.Password);
            cmd.AddParameter("@lastname", item.LastName);
            cmd.AddParameter("@firstname", item.FirstName);
            cmd.AddParameter("@birthdate", item.BirthDate);
            cmd.AddParameter("@number", item.Number);
            cmd.AddParameter("@street", item.Street);
            cmd.AddParameter("@zipcode", item.ZipCode);
            cmd.AddParameter("@City", item.City);
            return _Connection.ExecuteNonQuery(cmd);
        }

        public User Login(string email, string password)
        {
            Command cmd = new Command("Sp_Login", true);
            cmd.AddParameter("@email", email);
            cmd.AddParameter("@password", password);
            return _Connection
                .ExecuteReader(cmd, DbToEntityMapper.ToUser)
                .FirstOrDefault();
        }

        public override bool Update(User item)
        {
            throw new NotImplementedException();
        }
    }
}
