﻿using DB;
using Demo.DAL.Entities;
using Demo.DAL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Services
{
    public class CategoryService : BaseService<Category>
    {
        public override bool Delete(int id)
        {
            Command cmd = new Command("DELETE FROM Category WHERE Id = @id");
            cmd.AddParameter("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Category Get(int id)
        {
            Command cmd = new Command("SELECT * FROM Category WHERE Id = @id");
            cmd.AddParameter("@id", id);
            return _Connection
                .ExecuteReader(cmd, DbToEntityMapper.ToCategory)
                .FirstOrDefault();
        }

        public override IEnumerable<Category> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Category WHERE Deleted = 0");
            return _Connection
                .ExecuteReader(cmd, DbToEntityMapper.ToCategory);
        }

        public override int Insert(Category item)
        {
            Command cmd = new Command("INSERT INTO Category(Name) OUTPUT inserted.Id Values(@name)");
            cmd.AddParameter("@name", item.Name);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Category item)
        {
            Command cmd = new Command("UPDATE Category SET Name = @name WHERE Id = @id");
            cmd.AddParameter("@name", item.Name);
            cmd.AddParameter("@id", item.Id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }
    }
}
