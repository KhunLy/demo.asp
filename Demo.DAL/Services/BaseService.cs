﻿using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Services
{
    public abstract class BaseService<T>
    {
        protected Connection _Connection;
        public BaseService()
        {
            _Connection = new Connection(
                "Server=TECHNOBEL;Initial Catalog=DemoSushi;User Id=sa;Password=test1234=",
                "System.Data.SqlClient"
            );
        }

        public abstract T Get(int id);
        public abstract IEnumerable<T> GetAll();
        public abstract int Insert(T item);
        public abstract bool Update(T item);
        public abstract bool Delete(int id);
    }
}
