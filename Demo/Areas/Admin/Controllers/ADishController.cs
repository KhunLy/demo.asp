﻿using Demo.Areas.Admin.Models;
using Demo.DAL.Entities;
using Demo.DAL.Services;
using Demo.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.Areas.Admin.Controllers
{
    public class ADishController : Controller
    {
        [HttpGet]
        public ActionResult Create()
        {
            return View(new DishForm());
        }

        [HttpPost]
        public ActionResult Create(DishForm model)
        {
            if (ModelState.IsValid)
            {
                Dish toInsert = model.Map<Dish>();
                // récupérer le fichier image
                if(model.File != null)
                {
                    //on crée un nom unique pour le fichier
                    string name = Guid.NewGuid().ToString() + model.File.FileName;
                    string path = 
                        Path.Combine(Server.MapPath("~/Content/Images"), name);
                    // placer l'image dans un dossier
                    model.File.SaveAs(path);
                    // mémoriser l'emplacement du fichier
                    toInsert.Picture = name;
                }
                DishService service = new DishService();
                // Insérer en db
                service.Insert(toInsert);
                return RedirectToAction("Index", "Dish", new { Area = "" });
                
                
            }
            return View(model);
        }
    }
}