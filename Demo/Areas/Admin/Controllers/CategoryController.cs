﻿using Demo.Areas.Admin.Models;
using Demo.DAL.Services;
using Demo.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        public ActionResult Index()
        {
            CategoryService service = new CategoryService();
            IEnumerable<CategoryModel> model = service.GetAll()
                .Select(item => item.Map<CategoryModel>());
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CategoryService service = new CategoryService();
            service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}