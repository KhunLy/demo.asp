﻿using Demo.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Areas.Admin.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity
        {
            get
            {
                DishService service = new DishService();
                return service.GetCountByCategory(Id);
                //return service.GetAll()
                //    .Count(d => d.CategoryId == Id);
            }
        }
    }
}