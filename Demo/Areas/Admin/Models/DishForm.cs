﻿using Demo.DAL.Entities;
using Demo.DAL.Services;
using Demo.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Areas.Admin.Models
{
    public class DishForm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Discount { get; set; }

        // Permet de récupérer une image
        public HttpPostedFileBase File { get; set; }

        public int CategoryId { get; set; }

        // "Lazy loading" des categories
        private IEnumerable<CategoryModel> _AllCategories;

        public IEnumerable<CategoryModel> AllCategories
        {
            get
            {
                if (_AllCategories == null)
                {
                    CategoryService s = new CategoryService();
                    _AllCategories = s.GetAll()
                        .Select(c => c.Map<CategoryModel>());
                }
                return _AllCategories;
            }
        }
    }
}