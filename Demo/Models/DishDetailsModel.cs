﻿using Demo.DAL.Entities;
using Demo.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Models
{
    public class DishDetailsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public decimal Price { get; set; }
        public int? Discount { get; set; }
        public int CategoryId { get; set; }

        private Category _Category;
        public Category Category
        {
            get
            {
                return _Category = _Category ?? (new CategoryService()).Get(CategoryId);
                //if(_Category == null)
                //{
                //    CategoryService service = new CategoryService();
                //    _Category =  service.Get(CategoryId);
                //}
                //return _Category;
            }
        }
        public decimal PriceWithDiscount
        {
            get { return Price - (Price * (Discount ?? 0) / 100); }
        }
        public IEnumerable<Comment> Comments { get; set; }
    }
}