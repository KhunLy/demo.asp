﻿using Demo.DAL.Entities;
using Demo.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Models
{
    public class SearchFormModel
    {
        public string NameSushi { get; set; }
        public List<int> PickedCategories { get; set; }

        private IEnumerable<Category> _AllCategories;
        public IEnumerable<Category> AllCategories
        {
            get
            {
                if(_AllCategories == null)
                {
                    CategoryService service = new CategoryService();
                    _AllCategories = service.GetAll();
                }
                return _AllCategories;
            }
        }
    }
}