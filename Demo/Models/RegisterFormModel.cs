﻿using Demo.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Demo.Models
{
    public class RegisterFormModel
    {
        public int Id { get; set; }

        [EmailAddress]
        [Required]
        [MaxLength(255, ErrorMessage = "Le Champs Email est trop long")]
        [UniqueEmail]
        public string Email { get; set; }

        [Required]
        [Compare("ConfirmPassword")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Street { get; set; }
        public string Number { get; set; }

        [Required]
        [Range(1000, 9999)]
        public int ZipCode { get; set; }

        [Required]
        public string City { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BirthDate { get; set; }

        [Required]
        public string CondAgreement { get; set; }
    }
}