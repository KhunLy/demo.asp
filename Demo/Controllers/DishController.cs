﻿using Demo.DAL.Entities;
using Demo.DAL.Services;
using Demo.Mapper;
using Demo.Models;
using Demo.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.Controllers
{
    public class DishController : Controller
    {
        // GET: Dish
        public ActionResult Index()
        {
            // récupération d'un dish en utilisant un service de la DAL
            DishService dishService = new DishService();
            // on récupère une liste d'objets de la DAL 
            IEnumerable<Dish> collection = dishService.GetAll();
            // on transforme ces objets en objet en type model
            //List<DishDetailsModel> model = new List<DishDetailsModel>();
            //foreach(Dish d in collection)
            //{
            //    model.Add(d.Map<DishDetailsModel>());
            //}
            IEnumerable<DishDetailsModel> model = collection
                .Select(item => item.Map<DishDetailsModel>());

            return View(model); // je passe le nom de la vue
        }

        public ActionResult Details(int id)
        {
            // On instancie un DishService
            // Son rôle est de récupérer ou de modifier des infos dans la table Dish
            DishService dishService = new DishService();
            DishDetailsModel model = dishService
                .Get(id).Map<DishDetailsModel>();

            //// On mappe l'entité db en model de présentaion
            //DishDetailsModel model = new DishDetailsModel
            //{
            //    Id = dish.Id,
            //    Name = dish.Name,
            //    Price = dish.Price,
            //    CategoryId = dish.CategoryId,
            //    Discount = dish.Discount,
            //    Picture = dish.Picture
            //};

            // Génération de la vue et passage du model à la vue
            return View(model);
        }

        [CustomSecurity("Customer", "ADMIN")]
        public ActionResult Cart()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View(new SearchFormModel());
        }

        [HttpPost]
        public ActionResult AjaxSearch(SearchFormModel model)
        {
            //récupérer tous les plats
            DishService service = new DishService();
            IEnumerable<Dish> allDishes = service.GetAll();

            //fitre les plats en fonction des critères du formulaire
            IEnumerable<DishDetailsModel> results = allDishes.Where(
                    d => d.Name.ToLower().Contains(model.NameSushi?.ToLower() ?? string.Empty)
            //transforme les "Dish" en "DishDetailsModel"
            ).Select(d => d.Map<DishDetailsModel>());
            if(model.PickedCategories != null)
            {
                results
                    = results.Where(d => model.PickedCategories.Contains(d.CategoryId));
            }
            return PartialView(results);
        }
    }
}